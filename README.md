# sg-catalog: Science Gateways Catalog App

### Clone and install
```
git clone git@bitbucket.org:manuelrojas/sg-catalog.git
cd sg-catalog
npm install
```

### Configure client

You must set your own `clientKey` in `/sg-catalog/src/javascripts/app.js` for authentication:

```
var OAuthClients = {
  'sgci': {
    'tenant': {
      'id': '5603147879475572250-242ac113-0001-016',
    	'name': 'Science Gateways Community Institute',
    	'baseUrl': 'https://sgci.agaveapi.co/',
    	'code': 'sgci',
    	'contact': [{
    		'name': 'Manuel Rojas',
    		'email': 'mrojas@tacc.utexas.edu',
    		'url': '',
    		'type': 'admin',
    		'primary': true
    	}],
    	'_links': {
    		'self': {
    			'href': 'https://agaveapi.co/tenants/sgci'
    		},
    		'publickey': {
    			'href': 'https://sgci.agaveapi.co/apim/v2/publickey'
    		}
    	}
    },
    'clientKey': '', // <-- your own clientKey
    'callbackUrl': 'https://localhost:3000/', // <-- your callbackUrl (this is default for dev. See Run below)
    'scope': 'PRODUCTION'
  }
};
```


An easy way to register your client and obtain your `clientKey` is through the CLI (https://bitbucket.org/agaveapi/cli):
```
$ tenants-init
Please select a tenant from the following list:
[0] agave.prod
[1] araport.org
[2] designsafe
[3] iplantc.org
[4] irec
[5] irmacs
[6] sgci
[7] tacc.prod
[8] vdjserver.org
Your choice [3]: 6
You are now configured to interact with the APIs at https://sgci.agaveapi.co

$ clients-create -N "sg_client" -C 'https://localhost:3000/' -S
API username : yourusername
API password: yourpassword
Successfully created client sg_client
key: 2YR...
secret: NaH3...
$ auth-check -v
{
  "tenantid": "sgci",
  "baseurl": "https://sgci.agaveapi.co",
  "devurl": "",
  "apisecret": "NaH3...",
  "apikey": "2YR...", // <-- your clientKey
  "username": "yourusername",
  "access_token": "9af08...",
  "refresh_token": "",
  "created_at": "",
  "expires_in": "",
  "expires_at": ""
}
```
### Run
#### dev
`Builds and serves files over port 3000`
```
npm start
```

#### prod
`Builds and serves bundle files over port 5000`
```
npm gulp server production
```
