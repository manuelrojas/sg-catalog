'use strict';

require('angular');
require('agave-angularjs-sdk');
require('ngstorage');
require('angular-ui-router');
require('oauth-ng');
require('angular-translate');
require('angular-aria');
require('angular-animate');
require('angular-material');
require('angular-messages');
require('angular-toastr');

require('./controllers/home.js');
require('./controllers/login.js');
require('./services/message.js');

var app = angular.module('app', [
	'ui.router',
	'AgavePlatformScienceAPILib',
	'oauth',
	'pascalprecht.translate',
	'ngAnimate',
	'ngMaterial',
	'ngMessages',
	'toastr',
	'app.message',
	'app.home',
	'app.login'
]);

app.constant('categoryMap', {
      'Formal':{
        'Mathematics': {}
      },
      'Physical':{
        'Chemistry':{
          'Analytical': {},
          'Environmental': {},
          'Inorganic': {},
          'Nuclear': {},
          'Organic': {},
          'Physical': {},
          'Supramolecular': {},
          'Sustainable': {},
          'Theoretical': {},
          'Astrochemistry': {},
          'Biochemistry': {},
          'Crystallography': {},
          'Food': {},
          'Geochemistry': {},
          'Materials Science': {},
          'Molecular Physics': {},
          'Photochemistry': {},
          'Radiochemistry': {},
          'Stereochemistry': {},
          'Surface Science': {}
        },
        'Physics':{
          'Classical': {},
          'Modern': {},
          'Applied': {},
          'Experimental': {},
          'Theoretical': {},
          'Computational': {},
          'Atomic': {},
          'Condensed Matter': {},
          'Mechanics': {},
          'Molecular': {},
          'Nuclear': {},
          'Particle': {},
          'Plasma': {},
          'Quantum Field Theory': {},
          'Quantum Mechanics': {},
          'Special Relativity': {},
          'General Relativity': {},
          'Rheology': {},
          'String theory': {},
          'Thermodynamics': {}
        },
        'Earth Sciences':{
          'Climatology': {},
          'Ecology': {},
          'Edaphology': {},
          'Environmental Science': {},
          'Geodesy': {},
          'Geography': {},
          'Geology': {},
          'Geomorphology': {},
          'Geophysics': {},
          'Glaciology': {},
          'Hydrology': {},
          'Limnology': {},
          'Meteorology': {},
          'Oceanography': {},
          'Paleoclimatology': {},
          'Paleoecology': {},
          'Palynology': {},
          'Pedology': {},
          'Volcanology': {}
        },
        'Space Science':{
          'Astronomy': {},
          'Astrophysics': {},
          'Cosmology': {},
          'Galactic Astronomy': {},
          'Planetary Geology': {},
          'Planetary Science': {},
          'Stellar Astronomy': {}
        }
      },
      'Life':{
        'Biology':{
          'Anatomy': {},
          'Astrobiology': {},
          'Biochemistry': {},
          'Biogeography': {},
          'Biological Engineering': {},
          'Biophysics': {},
          'Behavioral Neuroscience': {},
          'Biotechnology': {},
          'Botany': {},
          'Cell': {},
          'Conservation': {},
          'Cryobiology': {},
          'Developmental': {},
          'Ethnobiology': {},
          'Ethology': {},
          'Evolutionary': {},
          'Genetics': {},
          'Gerontology': {},
          'Immunology': {},
          'Limnology': {},
          'Marine': {},
          'Microbiology': {},
          'Molecular': {},
          'Neuroscience': {},
          'Paleontology': {},
          'Parasitology': {},
          'Physiology': {},
          'Radiobiology': {},
          'Soil Science': {},
          'Sociobiology': {},
          'Systematics': {},
          'Toxicology': {},
          'Zoology': {}
        }
      },
      'Social':{
        'Anthropology': {},
        'Archaeology': {},
        'Criminology': {},
        'Demography': {},
        'Economics': {},
        'Geography': {},
        'History': {},
        'International Relations': {},
        'Law': {},
        'Linguistics': {},
        'Pedagogy': {},
        'Political Science': {},
        'Psychology': {},
        'Education': {},
        'Sociology': {}
      },
      'Applied': {
        'Engineering': {
          'Aerospace': {},
          'Agricultural': {},
          'Biological': {},
          'Biomedical': {},
          'Chemical': {},
          'Civil': {},
          'Computer Science': {},
          'Electrical': {},
          'Fire Protection': {},
          'Genetic': {},
          'Industrial': {},
          'Mechanical': {},
          'Military': {},
          'Mining': {},
          'Nuclear': {},
          'Operations Research': {},
          'Robotics': {},
        },
        'Healthcare':{
          'Medicine': {},
          'Veterinary': {},
          'Dentistry': {},
          'Midwifery': {},
          'Epidemiology': {},
          'Pharmacy': {},
          'Nursing': {},
        }
      },
      'Interdisciplinary': {
        'Applied Physics': {},
        'Artificial Intelligence': {},
        'Bioethics': {},
        'Bioinformatics': {},
        'Biomedical Engineering': {},
        'Biostatistics': {},
        'Cognitive Science': {},
        'Complex systems': {},
        'Computational Linguistics': {},
        'Cultural Studies': {},
        'Cybernetics': {},
        'Environmental Science': {},
        'Environmental Social Science': {},
        'Environmental Studies': {},
        'Ethnic Studies': {},
        'Evolutionary Psychology': {},
        'Forensics': {},
        'Forestry': {},
        'Library Science': {},
        'Military Science': {},
        'Network Science': {},
        'Neural Engineering': {},
        'Neuroscience': {},
        'Science Studies': {},
        'Scientific Modelling': {},
        'Semiotics': {},
        'Sociobiology': {},
        'Statistics': {},
        'Systems Science': {},
        'Urban Planning': {},
        'Web Science': {},
      },
      'Philosophy History':{
        'Basic Research': {},
        'Citizen Science': {},
        'Fringe Science': {},
        'Protoscience': {},
        'Pseudoscience': {},
        'Freedom': {},
        'Policy': {},
        'Funding': {},
        'Method': {},
        'Technoscience': {},
      }
});

app.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    allowHtml: false,
    autoDismiss: true,
    closeButton: true,
    maxOpened: 0,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: false,
    timeOut: 5000
  });
});

app.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/home');

	$stateProvider
		.state('access-token', {
			url: '/access_token=:accessToken',
			templateUrl: '',
			data: {pageTitle: 'OAuth Redirect'},
			controller: 'loginCtrl'
		})
		.state('home', {
      url: '/home',
			data: {pageTitle: 'Science Gateways Catalog'},
      templateUrl: 'views/home.html',
      controller: 'homeCtrl'
		});
});

app.config(function($translateProvider) {
  $translateProvider.translations('en', {
    'error_list_gateways': 'Error listing gateways',
    'error_delete_gateway': 'Error deleting gateway',
    'error_adding_gateway': 'Error adding gateway',
    'error_update_gateway': 'Error updating gateway',
    'error_login': 'Error logging in'
  });

  $translateProvider.preferredLanguage('en');
});

app.factory('settings', ['$rootScope', function($rootScope) {

	// TO-DO: move this out of here
	var OAuthClients = {
	  'sgci': {
	    'tenant': {
	      'id': '5603147879475572250-242ac113-0001-016',
	    	'name': 'Science Gateways Community Institute',
	    	'baseUrl': 'https://sgci.agaveapi.co/',
	    	'code': 'sgci',
	    	'contact': [{
	    		'name': 'Manuel Rojas',
	    		'email': 'mrojas@tacc.utexas.edu',
	    		'url': '',
	    		'type': 'admin',
	    		'primary': true
	    	}],
	    	'_links': {
	    		'self': {
	    			'href': 'https://agaveapi.co/tenants/sgci'
	    		},
	    		'publickey': {
	    			'href': 'https://sgci.agaveapi.co/apim/v2/publickey'
	    		}
	    	}
	    },
	    'clientKey': 'sk3dwBIGEqRaDC0z6CNPsyoNXRwa',
	    'callbackUrl': 'https://localhost:3000/',
	    'scope': 'PRODUCTION'
	  }
	};

  var settings = {
      oauth: OAuthClients
  };

  $rootScope.settings = settings;

  return settings;
}]);

app.run(['$rootScope', '$state', '$stateParams', '$localStorage', '$timeout', '$window', 'settings', function($rootScope, $state, $stateParams, $localStorage, $timeout, $window, settings){
	$rootScope.$state = $state; // state to be accessed from view
  $rootScope.$settings = settings; // state to be accessed from view

  // set default tacc tenant for now
  $localStorage.tenant = settings.oauth.sgci.tenant;

  $rootScope.logout = function(){
    $window.location.href = '/';
  };

  $rootScope.$on('oauth:login', function(event, token) {
      $localStorage.token = token;
  });

  $rootScope.$on('$stateChangeStart', function(){
      $rootScope.loggedIn = true;
      if (typeof $localStorage.tenant !== 'undefined' && typeof $localStorage.token !== 'undefined'){
        // // var currentDate = new Date();
        // // var expirationDate = Date.parse($localStorage.token.expires_at);
        // // var diff = (expirationDate - currentDate) / 60000;
        // // if (diff < 0) {
        // //   $window.location.href = '/';
        // // }
        // $rootScope.loggedIn = false;
      } else {
        $rootScope.loggedIn = false;
      }
  });
}]);
