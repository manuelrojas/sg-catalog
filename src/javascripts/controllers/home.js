'use strict';

angular.module('app.home', []).controller('homeCtrl', ['$scope', '$rootScope', '$localStorage', '$http', '$state', '$mdDialog', '$translate', '$filter', 'settings', 'toastr', 'MetaController', 'messageService', 'categoryMap',  function ($scope, $rootScope, $localStorage, $http, $state, $mdDialog, $translate, $filter, settings, toastr, MetaController, messageService, categoryMap) {
	$scope.settings = settings;

  $scope.filter = '';

  $scope.query = '{"name":"sg_catalog_gateway"}';

  $scope.category = {};
  $scope.subcategory = {};
  $scope.subcategory_children = {};

  $scope.search = function(){
    $scope.gateways = [];
    $scope.gateways = $filter('filter')($scope.gatewaysOriginal, $scope.filter);
    $scope.refreshCounters($scope.gateways);
  };

  $scope.refreshCounters = function(gateways){
    $scope.requesting = false;
    $scope.categories = [];
    $scope.categories = angular.copy(categoryMap);

   angular.forEach(gateways, function(gateway){
     angular.forEach(gateway.value.categories, function(category){
       $scope.categories[category.name].total = ($scope.categories[category.name].total || 0) + 1;
       angular.forEach(category.subcategory, function(subcategory){
         $scope.categories[category.name][subcategory.name].total = ($scope.categories[category.name][subcategory.name].total || 0) + 1;
         angular.forEach(subcategory.subcategory_children, function(subcategory_children){
           $scope.categories[category.name][subcategory.name][subcategory_children].total = ($scope.categories[category.name][subcategory.name][subcategory_children].total || 0) + 1;
         });
       });
     });
   });
 };

  $scope.filterCategory = function(category){
    $scope.gateways = [];
    $scope.filter = category;
    $scope.gateways = $filter('filter')($scope.gatewaysOriginal, $scope.filter);
  };

  $scope.filterTags = function(category){
    $scope.gateways = [];
    $scope.filter = category;
    $scope.gateways = $filter('filter')($scope.gatewaysOriginal, $scope.filter);
    $scope.refreshCounters($scope.gateways);
  };

  $scope.refresh = function(){
    $scope.requesting = true;

    MetaController.listMetadata($scope.query, 99999, 0)
      .then(
        function(response){
          $scope.gateways = [];
          angular.forEach(response.result, function(gateway){
            $scope.gateways.push(gateway);
          });

          $scope.gatewaysOriginal = angular.copy($scope.gateways);
          $scope.refreshCounters($scope.gateways);
          $scope.requesting = false;
        },
        function(response){
          $scope.requesting = false;
          messageService.handle(response, $translate.instant('error_list_gateways'));
        }
      );
  };

  $scope.add = function(){
    $scope.gateway = {'name': 'sg_catalog_gateway'};
    $scope.gateway.value = {};
    $scope.gateway.value.site = '';
    $scope.gateway.value.name = '';
    $scope.gateway.value.description = '';
    $scope.gateway.value.tags = ['my', 'awesome', 'tags'];

    $mdDialog.show({
      controller: function(){
        $scope.hide = function() {
          $mdDialog.hide();
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.save = function(gateway){
          $scope.requesting = true;
          MetaController.addMetadata(gateway)
            .then(
              function(response){
                $scope.gateways.push(response.result);

                $scope.requesting = false;
                $mdDialog.hide();
              },
              function(response){
                messageService.handle(response, $translate.instant('error_delete_gateway'));
                $scope.requesting = false;
              }
            );

        };
      },
      templateUrl: 'views/add.html',
      parent: angular.element(document.body),
      scope: this,
      clickOutsideToClose:true,
      preserveScope:true
    });

  };

  if ($scope.loggedIn){
        $scope.refresh();
  }


  $scope.openMenu = function($mdOpenMenu, ev) {
    $mdOpenMenu(ev);
  };

	function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.save = function(gateway) {
      $scope.requesting = true;
      // TO-DO: clean this up
      var body = {'name': 'sg_catalog_gateway'};
      body.value = {};
      body.value.name = gateway.value.name;
      body.value.description = gateway.value.description;
      body.value.site = gateway.value.site;
      body.value.tags = gateway.value.tags;
      MetaController.updateMetadata(body, gateway.uuid)
        .then(
          function(){
            $scope.requesting = false;
            $mdDialog.hide();
          },
          function(response){
            messageService.handle(response, $translate.instant('error_update_gateway'));
            $scope.requesting = false;
          }
        );
    };
  }

  $scope.action = function(ev, action, gateway){
    switch(action){
      case 'edit':
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'views/edit.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          scope: this,
          clickOutsideToClose:true,
          preserveScope:true
        });
        break;
      case 'delete':
        $scope.requesting = true;
        MetaController.deleteMetadata(gateway.uuid)
          .then(
            function(){
              $scope.gateways.splice($scope.gateways.indexOf(gateway), 1);
              $scope.requesting = false;
              $mdDialog.hide();
            },
            function(response){
              messageService.handle(response, $translate.instant('error_delete_gateway'));
              $scope.requesting = false;
            }
          );
          break;
      // case 'publish':
      // case 'unpublish':
      //   break;
    }
  };



}]);
