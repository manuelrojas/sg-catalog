'use strict';

angular.module('app.login',[]).controller('loginCtrl', ['$scope', '$rootScope', '$localStorage', '$location', '$timeout', '$window', '$state', '$translate', 'settings', 'AccessToken', 'Configuration', 'ProfilesController', 'messageService', function ($scope, $rootScope, $localStorage, $location, $timeout, $window, $state, $translate, settings, AccessToken, Configuration, ProfilesController, messageService) {

  var hash = $location.path().substr(1);
  AccessToken.setTokenFromString(hash);

  Configuration.oAuthAccessToken = $localStorage.token ? $localStorage.token.access_token : '';
  Configuration.BASEURI = $localStorage.tenant ? $localStorage.tenant.baseUrl : '';

  $scope.authToken = $localStorage.token;

  if (!!$scope.authToken) {
    $scope.profile = $localStorage.activeProfile;
    if (typeof $scope.profile === 'undefined') {
        $scope.requesting = true;
        ProfilesController.getProfile('me').then(
            function(response) {
                $localStorage.activeProfile = response;
                $scope.requesting = false;
                $scope.tenant = $localStorage.tenant;
                $window.location.href = '/';
            },
            function(response) {
              messageService.handle(response, $translate.instant('error_login'));
              $scope.requesting = false;
            }
        );
    } else {
      $window.location.href = '/';
    }
  } else {
    messageService.handle('', $translate.instant('error_login'));
  }

}]);
