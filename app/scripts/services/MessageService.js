angular.module('sgCatalogApp').service('MessageService',['$rootScope', 'toastr', function($rootScope, toastr){

  this.handle = function(response, message){
    var errorMessage = '';
    if (response.errorResponse){
      if (typeof response.errorResponse.message !== 'undefined') {
        errorMessage = message + ' - ' + response.errorResponse.message
      } else if (typeof response.errorResponse.fault !== 'undefined'){
        errorMessage = message + ' - ' + response.errorResponse.fault.message;
      } else {
        errorMessage = message;
      }
    } else {
      errorMessage = message;
    }
    toastr.info(errorMessage);
  };
}]);
