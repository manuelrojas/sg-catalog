var app = angular.module('sgCatalogApp', [
  'ui.router',
  'AgavePlatformScienceAPILib',
  'oauth',
  'oc.lazyLoad',
  'pascalprecht.translate',
  'ngAnimate',
  'ngMaterial',
  'ngMessages',
  'ui.materialize',
  'toastr'
]);

// TO-DO: Move this to a separate file
app.constant('categoryMap', {
      'Formal':{
        'Mathematics': {}
      },
      'Physical':{
        'Chemistry':{
          'Analytical': {},
          'Environmental': {},
          'Inorganic': {},
          'Nuclear': {},
          'Organic': {},
          'Physical': {},
          'Supramolecular': {},
          'Sustainable': {},
          'Theoretical': {},
          'Astrochemistry': {},
          'Biochemistry': {},
          'Crystallography': {},
          'Food': {},
          'Geochemistry': {},
          'Materials Science': {},
          'Molecular Physics': {},
          'Photochemistry': {},
          'Radiochemistry': {},
          'Stereochemistry': {},
          'Surface Science': {}
        },
        'Physics':{
          'Classical': {},
          'Modern': {},
          'Applied': {},
          'Experimental': {},
          'Theoretical': {},
          'Computational': {},
          'Atomic': {},
          'Condensed Matter': {},
          'Mechanics': {},
          'Molecular': {},
          'Nuclear': {},
          'Particle': {},
          'Plasma': {},
          'Quantum Field Theory': {},
          'Quantum Mechanics': {},
          'Special Relativity': {},
          'General Relativity': {},
          'Rheology': {},
          'String theory': {},
          'Thermodynamics': {}
        },
        'Earth Sciences':{
          'Climatology': {},
          'Ecology': {},
          'Edaphology': {},
          'Environmental Science': {},
          'Geodesy': {},
          'Geography': {},
          'Geology': {},
          'Geomorphology': {},
          'Geophysics': {},
          'Glaciology': {},
          'Hydrology': {},
          'Limnology': {},
          'Meteorology': {},
          'Oceanography': {},
          'Paleoclimatology': {},
          'Paleoecology': {},
          'Palynology': {},
          'Pedology': {},
          'Volcanology': {}
        },
        'Space Science':{
          'Astronomy': {},
          'Astrophysics': {},
          'Cosmology': {},
          'Galactic Astronomy': {},
          'Planetary Geology': {},
          'Planetary Science': {},
          'Stellar Astronomy': {}
        }
      },
      'Life':{
        'Biology':{
          'Anatomy': {},
          'Astrobiology': {},
          'Biochemistry': {},
          'Biogeography': {},
          'Biological Engineering': {},
          'Biophysics': {},
          'Behavioral Neuroscience': {},
          'Biotechnology': {},
          'Botany': {},
          'Cell': {},
          'Conservation': {},
          'Cryobiology': {},
          'Developmental': {},
          'Ethnobiology': {},
          'Ethology': {},
          'Evolutionary': {},
          'Genetics': {},
          'Gerontology': {},
          'Immunology': {},
          'Limnology': {},
          'Marine': {},
          'Microbiology': {},
          'Molecular': {},
          'Neuroscience': {},
          'Paleontology': {},
          'Parasitology': {},
          'Physiology': {},
          'Radiobiology': {},
          'Soil Science': {},
          'Sociobiology': {},
          'Systematics': {},
          'Toxicology': {},
          'Zoology': {}
        }
      },
      'Social':{
        'Anthropology': {},
        'Archaeology': {},
        'Criminology': {},
        'Demography': {},
        'Economics': {},
        'Geography': {},
        'History': {},
        'International Relations': {},
        'Law': {},
        'Linguistics': {},
        'Pedagogy': {},
        'Political Science': {},
        'Psychology': {},
        'Education': {},
        'Sociology': {}
      },
      'Applied': {
        'Engineering': {
          'Aerospace': {},
          'Agricultural': {},
          'Biological': {},
          'Biomedical': {},
          'Chemical': {},
          'Civil': {},
          'Computer Science': {},
          'Electrical': {},
          'Fire Protection': {},
          'Genetic': {},
          'Industrial': {},
          'Mechanical': {},
          'Military': {},
          'Mining': {},
          'Nuclear': {},
          'Operations Research': {},
          'Robotics': {},
        },
        'Healthcare':{
          'Medicine': {},
          'Veterinary': {},
          'Dentistry': {},
          'Midwifery': {},
          'Epidemiology': {},
          'Pharmacy': {},
          'Nursing': {},
        }
      },
      'Interdisciplinary': {
        'Applied Physics': {},
        'Artificial Intelligence': {},
        'Bioethics': {},
        'Bioinformatics': {},
        'Biomedical Engineering': {},
        'Biostatistics': {},
        'Cognitive Science': {},
        'Complex systems': {},
        'Computational Linguistics': {},
        'Cultural Studies': {},
        'Cybernetics': {},
        'Environmental Science': {},
        'Environmental Social Science': {},
        'Environmental Studies': {},
        'Ethnic Studies': {},
        'Evolutionary Psychology': {},
        'Forensics': {},
        'Forestry': {},
        'Library Science': {},
        'Military Science': {},
        'Network Science': {},
        'Neural Engineering': {},
        'Neuroscience': {},
        'Science Studies': {},
        'Scientific Modelling': {},
        'Semiotics': {},
        'Sociobiology': {},
        'Statistics': {},
        'Systems Science': {},
        'Urban Planning': {},
        'Web Science': {},
      },
      'Philosophy History':{
        'Basic Research': {},
        'Citizen Science': {},
        'Fringe Science': {},
        'Protoscience': {},
        'Pseudoscience': {},
        'Freedom': {},
        'Policy': {},
        'Funding': {},
        'Method': {},
        'Technoscience': {},
      }
});

app.config(function($mdIconProvider) {
  $mdIconProvider
    .iconSet("call", 'img/icons/sets/communication-icons.svg', 24)
    .iconSet("social", 'img/icons/sets/social-icons.svg', 24);
});

app.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: true
    });
}]);

app.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    allowHtml: false,
    autoDismiss: true,
    closeButton: true,
    maxOpened: 0,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: false,
    timeOut: 5000,
    // templates: {
    //   toast: 'directives/toast/toast.html',
    //   progressbar: 'directives/progressbar/progressbar.html'
    // },
  });
});

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

        .state('access-token', {
            url: '/access_token=:accessToken',
            templateUrl: '',
            data: {pageTitle: 'OAuth Redirect'},
            controller: 'LoginSuccessCtrl',
            resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                  {
                    name: 'sgCatalogApp',
                      files: [
                        'scripts/services/MessageService.js',
                        'scripts/controllers/LoginSuccessCtrl.js'
                      ]
                  }
                ]);
              }]
            }
        })

        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl',
            resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                  {
                    name: 'sgCatalogApp',
                      files: [
                        'scripts/services/MessageService.js',
                        'scripts/controllers/HomeCtrl.js'
                      ]
                  }
                ]);
              }]
            }
        })

});

app.config(function($translateProvider) {
  $translateProvider.translations('en', {
    error_list_gateways: 'Error listing gateways',
    error_delete_gateway: 'Error deleting gateway',
    error_adding_gateway: 'Error adding gateway',
    error_update_gateway: 'Error updating gateway',
    error_login: 'Error logging in'
  });

  $translateProvider.preferredLanguage('en');
});

app.factory('settings', ['$rootScope', function($rootScope) {
  var settings = {
      oauth: OAuthClients
  };

  $rootScope.settings = settings;

  return settings;
}]);

app.run(['$rootScope', '$state', '$stateParams', '$localStorage', '$timeout', '$window', 'settings', function($rootScope, $state, $stateParams, $localStorage, $timeout, $window, settings){
  $rootScope.$state = $state; // state to be accessed from view
  $rootScope.$settings = settings; // state to be accessed from view

  // set default tacc tenant for now
  $localStorage.tenant = settings.oauth['sgci'].tenant;

  $rootScope.logout = function(){
    $window.location.href = '/';
  }

  $rootScope.$on('oauth:login', function(event, token) {
      $localStorage.token = token;
  });

  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      $rootScope.loggedIn = true;
      if (typeof $localStorage.tenant !== 'undefined' && typeof $localStorage.token !== 'undefined'){
        // // var currentDate = new Date();
        // // var expirationDate = Date.parse($localStorage.token.expires_at);
        // // var diff = (expirationDate - currentDate) / 60000;
        // // if (diff < 0) {
        // //   $window.location.href = '/';
        // // }
        // $rootScope.loggedIn = false;
      } else {
        $rootScope.loggedIn = false;
      }
  });
}]);
