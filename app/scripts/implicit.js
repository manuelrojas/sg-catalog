var OAuthClients = {
  'sgci': {
    'tenant': {
      'id': '5603147879475572250-242ac113-0001-016',
    	'name': 'Science Gateways Community Institute',
    	'baseUrl': 'https://sgci.agaveapi.co/',
    	'code': 'sgci',
    	'contact': [{
    		'name': 'Manuel Rojas',
    		'email': 'mrojas@tacc.utexas.edu',
    		'url': '',
    		'type': 'admin',
    		'primary': true
    	}],
    	'_links': {
    		'self': {
    			'href': 'https://agaveapi.co/tenants/sgci'
    		},
    		'publickey': {
    			'href': 'https://sgci.agaveapi.co/apim/v2/publickey'
    		}
    	}
    },
    'clientKey': '',
    'callbackUrl': 'https://localhost:9000/',
    'scope': 'PRODUCTION'
  }
};
